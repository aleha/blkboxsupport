#+TITLE: blkboxsupport R package
#+OPTIONS: toc:nil num:nil


* Preamble						      :ignoreheading:
#+md: <!-- Readme.md is generated from Readme.org. Please edit that file -->

#+begin_src emacs-lisp :exports none :results none
  (defun readmeplot ()
    ""
    (file-name-nondirectory (org-babel-temp-file "./Readme-fig-" ".png")))
#+end_src



* TOC 							      :ignoreheading:
# #+TOC: headlines 2


* blkboxsupport R package

This package provides some functionality for easy reporting of
=blkbox= ([[https://github.com/gboris/blkbox][github]], [[https://cran.r-project.org/package=blkbox][cran]]) classification results.

As for now, the focus is on results from nested cross validation
(results from call to =blkboxNCV()=).



** Example Usage                                                   :noexport:

To be done.


** Installation

Installation directly from gitlab is possible, when the package
[[https://cran.r-project.org/package=devtools][devtools]] is installed.

As R dependencies you'll need the development version of
[[https://github.com/gboris/blkbox][blkbox]] installed directly from github.  To install this run

#+begin_src R :exports code :eval never
  library("devtools")
  install_github("gboris/blkbox")
#+end_src

Once the dependencies are installed, get this package with

#+begin_src R :exports code :eval never
  library("devtools")
  install_git("https://gitlab.gwdg.de/aleha/blkboxsupport")
#+end_src


** Roadmap
Next steps include:
- support for non-nested result (=blkboxCV()=)


* Local Variables for the *.md file 			      :ignoreheading:

#+begin_export markdown
<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
#+end_export

* Ignore Local Variables above 					   :noexport:

