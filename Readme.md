

<!-- Readme.md is generated from Readme.org. Please edit that file -->


# blkboxsupport R package

This package provides some functionality for easy reporting of
`blkbox` ([github](https://github.com/gboris/blkbox), [cran](https://cran.r-project.org/package=blkbox)) classification results.

As for now, the focus is on results from nested cross validation
(results from call to `blkboxNCV()`).

## Installation

Installation directly from gitlab is possible, when the package
[devtools](https://cran.r-project.org/package=devtools) is installed.

As R dependencies you'll need the development version of
[blkbox](https://github.com/gboris/blkbox) installed directly from github.  To install this run

```R
library("devtools")
install_github("gboris/blkbox")
```

Once the dependencies are installed, get this package with

```R
library("devtools")
install_git("https://gitlab.gwdg.de/aleha/blkboxsupport")
```

## Roadmap

Next steps include:

-   support for non-nested result (`blkboxCV()`)


<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
